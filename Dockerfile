FROM mcr.microsoft.com/dotnet/sdk:5.0 as builder
WORKDIR /src
COPY ./OptimalWarehouse ./OptimalWarehouse
COPY ./OptimalWarehouse.Test ./OptimalWarehouse.Test
COPY ./optimal-warehouse.sln ./
RUN dotnet build --nologo -c RELEASE \
        OptimalWarehouse/OptimalWarehouse.csproj && \
    dotnet publish --nologo -c RELEASE -o /app \
        OptimalWarehouse/OptimalWarehouse.csproj

FROM mcr.microsoft.com/dotnet/runtime:5.0
WORKDIR /app
COPY --from=builder /app /app
ENV IS_DOCKER=1
EXPOSE 80
CMD [ "dotnet", "/app/OptimalWarehouse.dll" ]
