#!/bin/bash

# requires jq

{
    echo "graph {";
    jq -r ".graph.edges[] | \"    \" + (.[0] | tostring) + \" -- \" + (.[1] | tostring) + \";\"" $1;
    echo "}"
} > "$(dirname $1)/$(basename $1 .json).dot"
