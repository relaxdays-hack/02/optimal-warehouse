---
geometry: margin=30mm
---
# Optimal Warehouse

## Aufgabe

Für die Originale Aufgabe siehe: [relaxdays-unternehmen.de/hackathon/portal](https://relaxdays-unternehmen.de/hackathon/portal/)  
Hier steht nur eine grobe Beschreibung die exakte Definition ist in `notes.md`.
\bigskip
Wir haben einen gitterförmigen Graph gegeben.  
Wir bekommen Objekte und lagern diese auf Knoten des Graphen.  
Dann bekommen wir Mengen von Objekten und sollen diese wieder von den Knoten aufsammeln.  
Ziel ist es die Gesamtstrecke aller Ein- und Auslagerungen zu minimieren.

## Installation
Anweisungen für das Bauen des Docker Containers und was dafür vorrausgesetzt wird.

## Usage
Definition der Aufrufe des Containers (sollte von der Aufgabe festgelegt sein)

## Ansatz
Unser Ansatz und Implementierungsanmerkungen sind in `notes.md` genauer erklärt.

## notes.md
`nodes.md` enthält die exakte Definition des Problems und Anmerkungen dazu, wie wir das lösen wollen.  
Sowohl `nodes.md` als auch `README.md` können zu schöneren PDFs durch `copile README` und `compile nodes` kompeliert werden.

## Support
git gud scrub

## Authors
Die drei Deppveloper :*

## License
GPL 3 or higher, see `LICENSE.md` or [gnu.org/licenses/gpl-3.0](https://www.gnu.org/licenses/gpl-3.0).

