---
geometry: margin=30mm
---
# Problemdefinition

Die Aufgabe besteht aus zwei Teilen:

1. Sortiere alle Objekte ein.
2. Suche mehrere Mengen von Produkten wieder auf Touren raus.

Das Ziel ist es nun die Summe der Länge aller Touren zu minimieren.

## Input

Für die Originale Aufgabe siehe: [relaxdays-unternehmen.de/hackathon/portal](https://relaxdays-unternehmen.de/hackathon/portal/)

* Anzahl von Knoten
* Gitterförmiger Graph, d.h. sei $l$ die Länge und $b$ die Breite des Warenhauses, also die Anzahl an Reihen in einer von zwei Richtungen, dann gilt:
  * Jeder Knoten (bis auf das Depot) lässt sich über dessen Position (Länge x Breite) (null) indizieren:  
    $$ k_{i,j},\ i\in \{0,l-1\},\ j\in﻿\{0,b-1\} \Leftrightarrow \text{Knoten an Position Länge }l\text{ Breite }b $$
  * Der Graph hat exakt $l\cdot b + 1$ Knoten.
  * Für alle $i\in \{0,l-1\},\ j\in﻿\{0,b-1\}﻿$ ist $k_{i,j}$ definiert (und zusätzlich existieren noch die Kanten zum Depot).
* Kapazität jedes Knotens (eine Zahl, ist für alle Knoten gleich)
* Liste aller Artikel, wobei jeder Artikel jeweils mit:
  * `article_id`
  * `inverse_size`: Anzahl wie viele dieser Artikel maximal in einem Knoten eingelagert werden können.
  * `amount`: Anzahl wie viele dieser Artikel eingelagert werden sollen.

## Output

* Einlagerung
* Touren
  
## Weitere Spezifikationen

* Die Knoten werden mit IDs $\in \{1, .., b*l+1\}$ übergeben.
* Jeder Knoten kann nur Artikel einer Art beinhalten.
* Am Anfang ist das Warenhaus leer.
* Die Kapazität einer Objektmenge ist egal.
* Alle Objektmengen sind sammt Objektmengen vorab bekannt.

# Implementierungsanmerkungen

## Daten

* Keine einzelnen Knoten Objekte
* Speichere Alle Daten der Knoten in dem Graphen Objekt
* Speichere den Graph nicht nach ID sondern geordnet nach dem gitter, da der Algorithmus so Cache Effizient wird.

## Algorithmus

* Wir arbeiten in drei Phasen:
  1. Baue kürzeste Wege Matrix (und weitere sinnvolle Metriken) auf.
  2. Verteile alle Objekte (heuristisch) auf Knoten.
  3. Berechne jeweils die Touren.

## Strategie

* Da alle Touren bekannt sind können diese einzeln verteilt werden.
* Es könnte sinnvoll sein den Graph zu clustern und darauf zu auchten, das jede Tour möglichst in nur einem Cluster verläuft.
  * k-means in einem Spektralgraphen ist für sowas sehr gut.
* Ein Aufbau eines kürzesten Wege Baums vom Depot könnte sinnvoll sein.
* Nutze für eine Tour nur noch Kanten entlang kürzester Wege (dann ist die Optimale Tour durch DP über die Permutation der Knoten) bestimmbar.
* Die Reihenfolge, in der die Touren bearbeitet werden ist ebenfalls relevant.
* Die kürzeste Wege Matrix lässt sich 
  * synchron mit Floyd-Warshall berechnen.
  * parallel mit einem alle zu alle kürzeste Weege Algorithmus
* Die Gitterstruktur kann bei der kürzesten Weege Suche hilfreich sein. Siehe "Ausnutzen der Gitterstruktur" Abschnitt.
* Die kürzeste Wege Matrix lässt sich (single threaded) mit Floyd-Warshall berechnen.
* Zur Berechnung der Touren können wir auch ein TSP oder VR Orakel benutzen (falls das zu langsam ist fallback auf unsere heuristische Lösung).

### Parallelitätsanmerkungen

* jede Phase kann innerhalb dieser parallel bearbeitet werden.
* Phase 1 muss vor Phase 2 komplett fertig sein. 
* Touren in Phase 3 können parallel berechnet werden.
* Eine Tour kann erst dann bearbeitet werden, wenn alle Produkte dieser Tour plaziert sind.

### Ausnutzen der Gitterstruktur

* Für die Itteration über jeden Knoten gibt es maximal 4 Nachbarn zu beachten.
* Durch Speichern der Knoten in dieser Gitterstruktur entspricht die Spreicherdarstellung gut dem Graphen und ist dadurch cache effizienter als das Speichern sortiert nach ID.

## Testfälle
